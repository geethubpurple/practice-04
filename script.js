var QUESTIONS_PER_TICKET = 20, MAX_MISTAKES_ALLOWED = 2;

function cutRandomElement(list) {
    return list.splice(Math.round(Math.random() * (list.length - 1)), 1)[0];
}

function keys(list) {
    return Object.keys(list).map(Number);
}

// STEP 1. Check if it will be enough tickets for all applicants
if (Math.floor(keys(questions).length / QUESTIONS_PER_TICKET) < keys(applicants).length) {
    throw 'Not enough tickets for all applicants';
}

// STEP 2. Make tickets array
var tickets = {}, questionIds = keys(questions);
for (var i = 0; i < keys(applicants).length; i++) {
    tickets[i + 1] = {
        questions: []
    };
    for (var j = 0; j < QUESTIONS_PER_TICKET; j++) {
        tickets[i + 1].questions.push(cutRandomElement(questionIds));
    }
}

// STEP 3. Let's exam
var exams = [];
var ticketIds = keys(tickets);
// iterate over applicants
keys(applicants).forEach(function (applicantId) {
    // take random ticket
    var ticketId = cutRandomElement(ticketIds), ticket = tickets[ticketId], answers = [], failedCount = 0;

    // iterate over questions
    ticket.questions.every(function (questionId) {
        var question = questions[questionId];

        if (Math.random() > (MAX_MISTAKES_ALLOWED + 0.5) / QUESTIONS_PER_TICKET) {
            answers.push(question.check);
        } else {
            failedCount++;
            answers.push(cutRandomElement(keys(question.answers).filter(function (item) {
                return item != question.check;
            })));
        }

        return failedCount <= MAX_MISTAKES_ALLOWED;
    });

    exams.push({
        applicant: applicantId,
        ticket: ticketId,
        answers: answers,
        passed: answers.length == 20 && failedCount <= MAX_MISTAKES_ALLOWED
    });
});

// STEP 4. Output results
